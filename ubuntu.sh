#!/bin/bash

ansible_roles="
devoperate.archive_utils \
devoperate.docker \
devoperate.docker_compose \
devoperate.gcloud \
devoperate.google_chrome \
devoperate.gpg \
devoperate.hadolint \
devoperate.helm \
devoperate.kubectl \
devoperate.nodejs \
devoperate.openfortivpn \
devoperate.shellcheck \
devoperate.terraform \
devoperate.virtualbox \
"
ansible_playbook_path="/tmp/ansible-playbook.yml"

apt-get update && apt-get install -y \
ca-certificates \
apt-transport-https \
python3 \
python3-pip

python3 -m pip install --upgrade pip

pip install \
ansible \
ansible-lint \
openshift \
docker \
lxml \
pipenv

ansible-galaxy install $ansible_roles

curl -LSso $ansible_playbook_path \
https://bitbucket.org/fresche/ws-setup/raw/master/assets/ansible-playbook.yml

ansible-playbook -b --ask-become-pass $ansible_playbook_path

rm -f $ansible_playbook_path

# VSCode
curl https://packages.microsoft.com/keys/microsoft.asc | gpg --dearmor > microsoft.gpg
mv microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg
sh -c 'echo "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main" > /etc/apt/sources.list.d/vscode.list'
apt-get update && apt-get install -y code

<#
This is a basic script that sets up a basic development workstation with
appropriate programs and Windows features for Fresche Solutions'
developers.

This script is appropriate for...
- Java development
- Nodejs development
- PHP development
- PowerShell scripting
- bash scripting
#>

# List of Chocolatey packages to install
$packages = @(
    "docker-desktop",
    "git",
    "gradle",
    "jdk8",
    "jetbrainstoolbox",
    "maven",
    "microsoft-teams",
    "nodejs-lts",
    "python",
    "slack",
    "vscode"
)

# List of PowerShell modules to install
$ps_modules = @(
    "posh-git"
)

# List of Windows Features to enable
$win_features = @(
    "Microsoft-Hyper-V",
    "Microsoft-Windows-Subsystem-Linux"
)

# PowerShell profile
$posh_profile = @"
Set-PSReadlineOption -BellStyle None

Import-Module posh-git

# Chocolatey profile
$ChocolateyProfile = `"$env:ChocolateyInstall\helpers\chocolateyProfile.psm1`"
if (Test-Path($ChocolateyProfile)) {
  Import-Module `"$ChocolateyProfile`"
}
"@

# Progress bar variables
$activity = "Fresche Development Workstation Setup (Windows)"
$percent_complete = 1

# Install Chocolatey
Write-Progress -Activity $activity -Status "Installing Chocolatey" -CurrentOperation "Downloading and installing Chocolatey..." -PercentComplete $percent_complete

Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) *>&1 | Out-Null
Write-Output "Installed Chocolatey package manager"; $percent_complete = 5

# Enable Windows features
foreach ($item in $win_features) {
    Write-Progress -Activity $activity -Status "Enabling Windows Optional Features" -PercentComplete $percent_complete

    Enable-WindowsOptionalFeature -Online -All -FeatureName $item -NoRestart *>&1 | Out-Null
    Write-Output "Enabled $item"

    $percent_complete += (20 / $win_features.Length)
}

# Install a list of packages
foreach ($item in $packages) {
    Write-Progress -Activity $activity -Status "Installing Programs" -CurrentOperation "Installing $item..." -PercentComplete $percent_complete

    choco install $item --yes *>&1 | Out-Null
    Write-Output "Installed $item"

    $percent_complete += (60 / $packages.Length)
}

# Install NuGet package provider (for PowerShell modules)
Install-PackageProvider -Name NuGet -MinimumVersion 2.8.5.201 -Force *>&1 | Out-Null
Write-Output "Added NuGet package provider"

# Install some PowerShell modules
foreach ($item in $ps_modules) {
    Write-Progress -Activity $activity -Status "Installing PowerShell Modules" -PercentComplete $percent_complete

    Install-Module -Force -Scope AllUsers -Name $item *>&1 | Out-Null
    Write-Output "Installed $item"

    $percent_complete += (10 / $ps_modules.Length)
}

# Create PowerShell profile if one doesn't already exist
if (!(Test-Path $PROFILE)) {
    New-Item -Path $PROFILE -Type File -Force
    Write-Output $posh_profile | Out-File -FilePath $PROFILE
}

# Propose a restart to the user
Write-Warning "`n`nA restart may be required, is it OK to do that now?"
Restart-Computer -Confirm

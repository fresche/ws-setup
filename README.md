# Workstation Setup Scripts

These scripts will do what the name says they'll do.

## One-liner commands to copy-paste!

**Windows (paste in PowerShell)**

```
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://bitbucket.org/fresche/ws-setup/raw/master/windows.ps1'))
```

**Linux (Ubuntu)**

```
curl -sL https://bitbucket.org/fresche/ws-setup/raw/master/ubuntu.sh | sudo bash
```

**Mac (paste in Terminal)**

```
curl -sL https://bitbucket.org/fresche/ws-setup/raw/master/macos.sh | bash
```

## Documentation

See the confluence page associated with this repo [here](https://fresche.atlassian.net/wiki/spaces/PMM/pages/363692248/How+to+Setup+your+Workstation).

## Testing

The `Vagrantfile` in this repo contains VM definitions for sample systems that get automatically provisioned with the appropriate script.